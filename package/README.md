# Combo

Combo lets you build static APIs.

This is achieved by processing a given data source into a folder structure containing JSON files (without extensions). This folder structure can be hosted on services such as GitLab Pages.

## Name

The usefulness of this library comes from its ability to combine raw data with transformations in order to create useful, static APIs.

## Notice

Combo is in **early development**. It will contain bugs and is subject to change at any time.

## Installation

```bash
npm install combo
```

## Example

```ts
import Combo from "combo";

type DataSource = {
  id: string;
  title: string;
}[];

const api = new Combo<DataSource>();

const v1 = api.addVersion("v1");
v1.addRouteFunction("list", (data) => {
  return data.reduce((obj, { id, title }) => {
    return { ...obj, [id]: title };
  }, {});
});

api.build([
  { id: "001", title: "Test 001" },
  { id: "002", title: "Test 002" },
  { id: "003", title: "Test 003" },
]);
```
