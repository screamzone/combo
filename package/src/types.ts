import { Version } from "./classes/Version";

export type GenericRouteOutput = Record<string, any> | any[];

export type GenericRouteBuilder<T, U extends GenericRouteOutput = GenericRouteOutput> = (data: T) => Record<string, U>;

export type GenericRouteFunction<T, U extends GenericRouteOutput = GenericRouteOutput> = (data: T) => U;