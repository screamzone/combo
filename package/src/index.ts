import { Combo } from "./classes/Combo";
import type { GenericRouteBuilder, GenericRouteFunction, GenericRouteOutput } from "./types";

export default Combo;

export type {
  GenericRouteBuilder,
  GenericRouteFunction,
  GenericRouteOutput,
}