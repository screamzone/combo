import { GenericRouteBuilder, GenericRouteFunction, GenericRouteOutput } from "../types";
import { Route, RouteResult } from "./Route";

// Builder paths end with "/", route paths do not.
// Builders do not allow other builders or functions to be nested within their paths.

export class Version<T> {
  protected routes: Record<string, GenericRouteBuilder<T, GenericRouteOutput>> = {};

  constructor (public prefix: string) {}

  public addRouteBuilder<U extends GenericRouteOutput>(path: string, fn: GenericRouteBuilder<T, U>) {
    const route = path + "/";

    if (!this.isRouteAvailable(route)) {
      throw new Error(`Can't create route builder as path "${path}" is occupied.`);
    }

    this.routes[route] = (data: T) => {
      const result = fn(data);
      return Object.keys(result).reduce((obj, key) => ({
        ...obj,
        [`${this.prefix}/${path}/${key}`]: result[key],
      }), {});
    };
  }

  public addRouteFunction<U extends GenericRouteOutput>(path: string, fn: GenericRouteFunction<T, U>) {
    if (!this.isRouteAvailable(path)) {
      throw new Error(`Can't create route function as path "${path}" is occupied.`);
    }

    this.routes[path] = (data: T) => ({
      [`${this.prefix}/${path}`]: fn(data),
    });
  }

  public buildRoutes(data: T): Record<string, RouteResult> {
    return Object.keys(this.routes).reduce((output, key) => ({
      ...output,
      ...this.routes[key](data),
    }), {} as Record<string, RouteResult>);
  }

  public isRouteAvailable(path: string): boolean {
    // Builders must own their directories.
    if (path.endsWith("/")) {
      return !Object.keys(this.routes).some((route) => route.startsWith(path));
    }

    return !Object.keys(this.routes).some((route) => {
      return route.endsWith("/")
        ? path.startsWith(route)
        : route === path;
    });
  }
}