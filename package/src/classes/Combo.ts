import { existsSync, mkdirSync, rmSync, writeFileSync } from "fs";
import { RouteResult } from "./Route";
import { Version } from "./Version";

export interface ComboProps {
  baseUrl: string;
  buildFolder: string;
  cleanOnBuild: boolean;
}

/**
 * Represents an API <rewrite this>.
 */
export class Combo<T> {
  // protected baseUrl: string = "";
  protected buildFolder: string = "public";
  protected cleanOnBuild: boolean = true;
  protected versions: Version<T>[] = [];

  constructor(props?: Partial<ComboProps>) {
    // this.baseUrl = props?.baseUrl ?? this.baseUrl;
    this.buildFolder = props?.buildFolder ?? this.buildFolder;
    this.cleanOnBuild = props?.cleanOnBuild ?? this.cleanOnBuild;
  }

  public addVersion(prefix: string): Version<T> {
    if (this.versions.some((version) => version.prefix === prefix)) {
      throw new Error(`Can't create duplicate version with prefix "${prefix}"`);
    }

    const version = new Version<T>(prefix);
    this.versions.push(version);
    return version;
  }

  public build(data: T) {
    const routes = this.versions.reduce((obj, version) => {
      return {
        ...obj,
        ...version.buildRoutes(data),
      };
    }, {} as Record<string, RouteResult>);

    if (this.cleanOnBuild && existsSync(this.buildFolder)) {
      rmSync(this.buildFolder, {
        force: true,
        recursive: true,
      });
    }

    mkdirSync(this.buildFolder);

    for (const key of Object.keys(routes)) {
      const folders = key.split("/");
      const file = folders.pop();
      const path = this.buildFolder + "/" + folders.join("/");

      mkdirSync(path, {
        recursive: true,
      });

      writeFileSync(path + "/" + file, JSON.stringify(routes[key]));
    }
  }
}