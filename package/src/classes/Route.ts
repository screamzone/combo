export type RouteResult = Record<any, any> | any[];

export class Route<T> {
  constructor (
    public path: string,
    public build: (data: T) => RouteResult,
  ) {}
}